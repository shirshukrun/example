import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';

import {MatCardModule} from '@angular/material/card';
import { RouterModule, Routes } from '@angular/router';
import { AngularFireAuthModule } from '@angular/fire/auth';//add in imports
import { AngularFireAuth } from '@angular/fire/auth';
import { ArticlesComponent } from './articles/articles.component';//add in imports


const appRoutes: Routes = [
 // { path: 'books', component: BooksComponent },
 // { path: 'temperatures', component: TemperaturesComponent},
  //{ path: '', //אם אין משהו אחרי הסלאש
  //  redirectTo: '/books', //מנתב לבוקס 
    //pathMatch: 'full' //אנחנו צריכים התאמה מדויקת
 // },
];


@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    NavComponent,
    ArticlesComponent
  ],
  imports: [
    BrowserModule,    
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    RouterModule.forRoot(
      appRoutes,{enableTracing: true } // <-- debugging purposes only
    ) 
    

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
